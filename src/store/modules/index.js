import Auth from "./auth.store";
import Collections from "./collections.store";
import Tasks from "./tasks.store";

export default {
    Auth,
    Collections,
    Tasks,
};